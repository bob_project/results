"""
This script is only used to print data in copy/paste ready format for a table software (ie LibreOffice, Excel, Google Sheet).
Yes, mathplotlib can produce some awesome bar charts, but it takes way more time. :(
"""

def print_data(classic_dic, shadow_dict):
    keys = ['MAJORITY', 'MFIRST', 'CREDIB']
    i = 0
    for k in keys:
        res = {}
        for rate in classic_dic[k]:
            diff = classic_dic[k][rate] - shadow_dict[k][rate]
            res[rate] = diff
        print(k)
        for v in list(res.values()):
            print(v)

# data for false positives
classic_dic = {'MAJORITY': {0: 0.0, 0.05: 0.0, 0.1: 1.5, 0.2: 17.333333333333332, 0.3: 15.833333333333334, 0.4: 12.166666666666668, 0.5: 7.333333333333333},
                'MFIRST': {0: 0.0, 0.05: 0.0, 0.1: 0.0, 0.2: 16.0, 0.3: 15.5, 0.4: 11.5, 0.5: 7.5},
                'CREDIB': {0: 0.0, 0.05: 0.0, 0.1: 0.16666666666666666, 0.2: 4.0, 0.3: 8.5, 0.4: 8.166666666666668, 0.5: 7.333333333333333}}
shadow_dict = {'MAJORITY': {0: 0.0, 0.05: 0.0, 0.1: 0.0, 0.2: 9.166666666666666, 0.3: 13.166666666666666, 0.4: 10.166666666666666, 0.5: 7.833333333333333},
                'MFIRST': {0: 0.0, 0.05: 0.0, 0.1: 0.0, 0.2: 7.166666666666667, 0.3: 11.833333333333334, 0.4: 9.166666666666666, 0.5: 6.5},
                'CREDIB': {0: 0.0, 0.05: 0.0, 0.1: 0.0, 0.2: 4.666666666666667, 0.3: 3.0, 0.4: 2.833333333333333, 0.5: 2.833333333333333}}
# print_data(classic_dic, shadow_dict)

# data for false negatives
classic_dic = {'MAJORITY': {0: 0.0, 0.05: 2.5, 0.1: 4.0, 0.2: 10.0, 0.3: 15.0, 0.4: 20.0, 0.5: 26.0},
                'MFIRST': {0: 0.16666666666666666, 0.05: 3.0, 0.1: 4.666666666666666, 0.2: 9.666666666666668, 0.3: 15.166666666666668, 0.4: 20.166666666666664, 0.5: 26.166666666666664},
                'CREDIB': {0: 0.16666666666666666, 0.05: 2.1666666666666665, 0.1: 3.5, 0.2: 9.166666666666666, 0.3: 13.0, 0.4: 17.833333333333332, 0.5: 22.833333333333336}}
shadow_dict = {'MAJORITY': {0: 0.0, 0.05: 0.3333333333333333, 0.1: 0.3333333333333333, 0.2: 6.833333333333333, 0.3: 13.833333333333332, 0.4: 20.0, 0.5: 26.0},
                'MFIRST': {0: 0.0, 0.05: 0.3333333333333333, 0.1: 0.3333333333333333, 0.2: 5.166666666666667, 0.3: 14.666666666666668, 0.4: 19.0, 0.5: 24.5},
                'CREDIB': {0: 0.16666666666666666, 0.05: 0.16666666666666666, 0.1: 0.16666666666666666, 0.2: 1.3333333333333333, 0.3: 1.5, 0.4: 4.166666666666667, 0.5: 5.666666666666667}}
#print_data(classic_dic, shadow_dict)


# data for number of task replication
def print_data_project(png, hash):
    res = {}
    for rate in png:
        # flat difference
        diff = png[rate] - hash[rate]
        # percentage of change between png and hash
        if(hash[rate] != 0):
            perc = png[rate] / hash[rate] * 100
            print(perc)

        res[rate] = diff
    print(res)
    avg = 0
    for v in list(res.values()):
        avg += v
    avg = avg / len(res)
    print(f"Average difference: {avg}")

# python3 exploit_results.py -f gb -gb project -d task_replications
png = {0: 370.3333333333333, 0.05: 378.11111111111114, 0.1: 374.8333333333333, 0.2: 369.1666666666667, 0.3: 367.6111111111111, 0.4: 362.9444444444444, 0.5: 370.4444444444445}
hash = {0: 143.27777777777774, 0.05: 147.27777777777777, 0.1: 150.2222222222222, 0.2: 155.44444444444443, 0.3: 157.88888888888889, 0.4: 161.11111111111111, 0.5: 161.61111111111111}
print_data_project(png, hash)


# python3 exploit_results.py -f gb -gb project -d shadow_count
png = {0: 0.0, 0.05: 15.666666666666666, 0.1: 23.722222222222225, 0.2: 56.22222222222223, 0.3: 54.111111111111114, 0.4: 52.388888888888886, 0.5: 43.0}
hash = {0: 0.0, 0.05: 3.2222222222222228, 0.1: 6.611111111111111, 0.2: 11.055555555555555, 0.3: 11.0, 0.4: 9.055555555555555, 0.5: 6.222222222222221}
print_data_project(png, hash)
