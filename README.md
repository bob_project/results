The data produced during our runs. Either exploit them yourself, use our `exploit_results.py` script or build your own.

# Caution
`exploit_results.py` uses the `eval` function with some user-provided input. Do not SUID the program (for security reasons).

# Usage
```
python3 exploit_results.py --help
```

Note: ROC curves are ugly and blatantly useless in our case. We should've stopped for like, 30 seconds, and think about it before implementing all the relevant functions.
Instead, check:
```
python3 exploit_results.py -f gb -d false_negative
python3 exploit_results.py -f gb -d false_positive
```

# Figures
Some `figures` have been uploaded already.

false_negative-GB_SHADOW.png:
```
python3 exploit_results.py -f gb -gb shadow -d false_negative  -y "Number of false negatives"
```

false_positive-GB_SHADOW.png:
```
python3 exploit_results.py -f gb -gb shadow -d false_positive  -y "Number of false positives"
```

global_load-GB_SHADOW.png:
```
python3 exploit_results.py -f gb -gb shadow -d global_load -y "Global load (bytes)"
```

shadow_count-GB_PROJECT.png:
```
python3 exploit_results.py -f gb -gb project -d shadow_count -y "Total number of computations done after shadow bans"
```

task_replications-GB_SHADOW.png:
```
python3 exploit_results.py -f gb -gb shadow -d task_replications -y "Number of task replications"
```

total_backtrack-GB_SHADOW.png:
```
python3 exploit_results.py -f gb -gb shadow -d total_backtrack  -y "Number of backtrack operations"
```
